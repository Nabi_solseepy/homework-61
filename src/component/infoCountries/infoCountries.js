import React from 'react';
import './infoCountries.css'


const InfoCountries = (props) => {
        if(!props.info) return null;
        return (
            <div className="infocountries clearfix">
                <div className="info">
                    <p>{props.info.name}</p>
                    <p>Capital: {props.info.capital}</p>
                    <p>Population: {props.info.population} M</p>
                        <p>Borders wicth:</p>

                        <ul>
                        {props.info.borders.map( (border, id) => {
                        return <li key={id}>{border}</li>
                        })}
                        </ul>

                </div>


                   <div className="flag">
                       <img className="flags" src={props.info.flag} alt=""/>

                   </div>



            </div>
        );
    };


export default InfoCountries;