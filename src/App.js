import React, { Component,Fragment } from 'react';
import './App.css';
import axios from 'axios'
import NameCountries from "./component/NameCountries/NameCountries";
import InfoCountries from "./component/infoCountries/infoCountries";

class App extends Component {

  state = {
    NameCountries: [],
      country: null
  };
  componentDidMount() {
      axios.get('https://restcountries.eu/rest/v2/all?fields=name;alpha3Code').then(response =>{
          this.setState({NameCountries: response.data})
      })
  }

  getCountryInfo = (name) => {
      axios.get(`https://restcountries.eu/rest/v2/name/${name}`).then(response => {
          const country = response.data[0];
          Promise.all(country.borders.map(border => axios.get(`https://restcountries.eu/rest/v2/alpha/${border}`)))
              .then(borderResponse => {
                  country.borders = borderResponse.map(border => border.data.name);
                  this.setState({
                      country: country
                  })
              })
      })
  };


    render() {
    return (
<Fragment >
        <div className="block">
            {this.state.NameCountries.map((country, id) => {
                return <NameCountries getInfo={() => this.getCountryInfo(country.name)} key={id} name={country.name} />
            })}

        </div>
    <div className="block-info"> <InfoCountries info={this.state.country}/></div>
</Fragment>

    );
  }
}

export default App;
